### Twitter Proxy
> This app is to get the twtter username and id from their developer api


### Rocket Config
All configuration parameters, including extras, can be overridden through environment variables. To override the configuration parameter {param}, use an environment variable named ROCKET_{PARAM}. For instance, to override the "port" configuration parameter, you can run your application with:
https://rocket.rs/v0.4/guide/configuration/
https://github.com/SergioBenitez/Rocket/blob/master/examples/config/Rocket.toml
https://github.com/SergioBenitez/Rocket/blob/master/examples/config/src/main.rs


### Running
```bash
cargo build --release

# replace the <bearer token> from twitter developer account
ROCKET_TOKEN=<bearer token> ./target/release/partisia-twitter-proxy

# or set it in Rocket.toml under global
token = "<bearer token>"
```
