#[macro_use]
extern crate rocket;
use std::convert::TryInto;
use std::str::FromStr;

use anyhow::{ensure, Result};
use dotenv::dotenv;
use rocket::response::Responder;
use rocket::serde::{json::Json, Deserialize, Serialize};
use sqlx::pool::PoolOptions;
use sqlx::postgres::{PgConnectOptions, PgPool, PgPoolOptions};
use sqlx::{Postgres, ConnectOptions};

type DB = rocket::State<PgPool>;

#[derive(Debug, Serialize, Deserialize)]
struct ResErr {
    message: String,
}

#[derive(Responder)]
enum ResponseJson {
    #[response(status = 200, content_type = "json")]
    Success(Json<()>),
    #[response(status = 500, content_type = "json")]
    Error(Json<ResErr>),
}
#[derive(Debug, Deserialize, Serialize)]
struct Post {
    address: String,
    confirm: bool,
}
#[get("/ping")]
pub async fn ping() -> &'static str {
    "pong"
}

#[post("/terms_accept", data = "<post>")]
async fn terms(post: Json<Post>, db: &DB) -> ResponseJson {
    match add_to_db(&post, &db).await {
        Ok(_) => ResponseJson::Success(Json(())),
        Err(e) => ResponseJson::Error(Json(ResErr {
            message: e.to_string(),
        })),
    }
}

async fn add_to_db(post: &Post, db: &DB) -> Result<()> {
    ensure!(post.confirm, "You must accept the terms and conditions");
    let address: [u8; 20] = hex::decode(&post.address.trim_start_matches("0x"))?
        .as_slice()
        .try_into()?;
    let _ = sqlx::query!(
        r#"
        insert into terms_accept (address, accept)
        values ($1, $2)
        ON CONFLICT DO NOTHING
        "#,
        address.as_slice(),
        post.confirm,
    )
    .execute(db.inner())
    .await?;
    Ok(())
}

#[rocket::main]
async fn main() -> Result<()> {
    dotenv().ok();

    // connect to the database
    // You can try to use PgConnectOptions to define a hostaddr to 172.17.0.1 and a PgSslMode::Disable instead.
    let db_pool: PgPool = {
        let pg_pool_options: PoolOptions<Postgres> = PgPoolOptions::new();
        let mut db_pool_options = PgConnectOptions::from_str(
            &dotenv::var("DATABASE_URL").expect("Must supply a DATABASE_URL in .env file"),
        )?;
        db_pool_options.disable_statement_logging();
        pg_pool_options.connect_with(db_pool_options).await?
    };

    // // Create all the tables in Postgres if not there for first time running
    // migrate!().run(&db_pool).await?;
    let api_prefix = "/tge";
    let server = rocket::build()
        .manage::<PgPool>(db_pool)
        .mount(api_prefix, routes![ping, terms])
        .ignite()
        .await
        .expect("unable to ignite server");
    let msg = format!(
        "API Server listening on {}:{}{} with {} workers",
        server.config().address,
        server.config().port,
        &api_prefix,
        server.config().workers
    );
    println!("{}", msg);
    server.launch().await?;
    Ok(())
}
