create table terms_accept(
    address bytea primary key,
    accept boolean not null,
	created_at int not null default EXTRACT(EPOCH FROM CURRENT_TIMESTAMP)
)